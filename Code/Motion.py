import RPi.GPIO as GPIO
import time
Green=18
Red=25
Blue=4
Button=24
Motion=17
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 
GPIO.setup(Red,GPIO.OUT)
GPIO.setup(Blue,GPIO.OUT)
GPIO.setup(Button,GPIO.IN)
GPIO.setup(Motion,GPIO.IN)
while 1==1:
    
    if GPIO.input(Motion):
       GPIO.output(Red, True)
       GPIO.output(Green,False) 
       # change code so that when motion is detected the blue light turns on but flashes for only half a second. 
    else:
     GPIO.output(Red, False)
     GPIO.output(Green,True)

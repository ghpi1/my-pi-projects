import RPi.GPIO as GPIO
import time
Green=18
Red=25
Blue=4
Button=24
Motion=17
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 
GPIO.setup(Red,GPIO.OUT)
GPIO.setup(Blue,GPIO.OUT)
GPIO.setup(Button,GPIO.IN)
GPIO.setup(Motion,GPIO.IN)


print "Blue On"
GPIO.output(Blue, True)
raw_input("Confirm")
GPIO.output(Blue, False)

print "Green On"
GPIO.output(Green, True)
raw_input("Confirm")
GPIO.output(Green, False)

print "Red On"
GPIO.output(Red, True)
raw_input("Confirm")
GPIO.output(Red, False)

while True:
    print GPIO.input(Button)
    if GPIO.input(Button):
       GPIO.output(Red, True)
       GPIO.output(Green,False) 
    else: 
        GPIO.output(Red, False)
        GPIO.output(Green,True)
        

import RPi.GPIO as GPIO
import time
import twitter

Green=18
Red=25
Blue=4
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 
GPIO.setup(Red,GPIO.OUT)
GPIO.setup(Blue,GPIO.OUT)

def been_mentioned(): 
	api = twitter.Api(consumer_key='', 
					  consumer_secret='', 
					  access_token_key='', 
					  access_token_secret='')
	mentions = api.GetMentions()
	most_recent = mentions[0]
	time_most_recent = most_recent.GetCreatedAtInSeconds()
	
	if (time.time() - time_most_recent) < 61:
		return 1
	else:
		return 0

def flash_positive():
	for i in range(5):
		GPIO.output(Blue, 1)
		time.sleep(.25)
		GPIO.output(Blue, 0)
		GPIO.output(Green, 1)
		time.sleep(.25)
		GPIO.output(Green, 0)
    
def flash_negative():
	for i in range(5):
		GPIO.output(Red, 1)
		time.sleep(.25)
		GPIO.output(Red, 0)
		time.sleep(.25)
		
while True:
	result = been_mentioned()
	if result == 1:
		print 'heyo'
		##flash_positive()
	else:
		print 'nopesters'
		##flash_negative()
		
	time.sleep(60)
	
	
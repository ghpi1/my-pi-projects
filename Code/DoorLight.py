import RPi.GPIO as GPIO
import time
Green=18
Red=25
Button=24
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 
GPIO.setup(Red,GPIO.OUT)
GPIO.setup(Button,GPIO.IN)
## Everything off
while True:
    if GPIO.input(Button):
       GPIO.output(Red, True)
       GPIO.output(Green,False) 
    else: 
        print 'Button not pressed'
    # change the code so that when the button is not pressed Red is off and Green is on. 
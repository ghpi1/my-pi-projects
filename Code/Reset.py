import RPi.GPIO as GPIO
import time
Green=18
Red=25
Button=24
Blue=4
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 
GPIO.setup(Red,GPIO.OUT)
GPIO.setup(Blue,GPIO.OUT)
GPIO.setup(Button,GPIO.IN)
## Everything off
GPIO.output(Red, False)
GPIO.output(Green,False)
GPIO.output(Blue,False)
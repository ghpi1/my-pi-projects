# most from weather.py

import pywapi
import string
import pprint
import time
import RPi.GPIO as GPIO
Green=18
Red=25
Blue=4
Button=24
Motion=17
ON = 1
OFF = 0
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 
GPIO.setup(Red,GPIO.OUT)
GPIO.setup(Blue,GPIO.OUT)
GPIO.setup(Button,GPIO.IN)
GPIO.setup(Motion,GPIO.IN)

places = ['KLAX','KDTW','KMSP','KPDX']
annarbor = "KARB"

# Potential: Change the code to flash different colors given different 
# situations at different times.

# indication of weather for several other places, dep on places list above
for p in places:
  weat = pywapi.get_weather_from_noaa(annarbor)
  print "outside temp at %s is" % (p), weat['temp_f']

  if float(weather['temp_f']) < 32:
    print "cold"
    GPIO.output(Blue, ON)
    time.sleep(3)
    GPIO.output(Blue, OFF)
  elif float(weat['temp_f']) > 32 and float(weat['temp_f']) < 65:
    print "pretty okay"
    GPIO.output(Green, ON)
    time.sleep(3)
    GPIO.output(Green,OFF)
  else:
    print 'hot'
    GPIO.output(Red, ON)
    time.sleep(3)
    GPIO.output(Red, OFF)

# Ann Arbor weather indicator
weather = pywapi.get_weather_from_noaa(annarbor)
print "outside temp is", weather['temp_f']

if float(weather['temp_f']) < 32:
    print "cold"
    GPIO.output(Blue, ON)
    time.sleep(4)
    GPIO.output(Blue, OFF)
elif float(weather['temp_f']) > 32 and float(weather['temp_f']) < 65:
  print "pretty okay"
  GPIO.output(Green, ON)
  time.sleep(4)
  GPIO.output(Green,OFF)
else:
   print 'hot'
   GPIO.output(Red, ON)
   time.sleep(4)
   GPIO.output(Red, OFF)

# get input from button and change what happens
while True:
    if GPIO.input(Button):
       GPIO.output(Red, True)
       GPIO.output(Green,False) 
    else: 
        print 'Button not pressed'
    # change the code so that when the button is not pressed Red is off and Green is on. 
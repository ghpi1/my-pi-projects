import RPi.GPIO as PI
import time
Green=18
Red=25
Blue=4
Button=24 # will be used for another program don't change
Motion=17 # will be used for another program don't change 
PI.setwarnings(False)
PI.setmode(PI.BCM)
PI.setup(Green, PI.OUT) 
PI.setup(Blue, PI.OUT)
PI.setup(Red, PI.OUT)
# Add code to setup the red light
# Add code to setup the blue light. 

#change the location of each light to a working connection on the bread board and change the python code accordingly so the lights still light up.

while True:
    PI.output(Green, True)
    time.sleep(.1)
    PI.output(Green, False)
    PI.output(Blue, True)
    time.sleep(.1)
    PI.output(Blue, False)
    PI.output(Red, True)
    time.sleep(.1)
    PI.output(Red, False)
 
# Change the code so that the lights flash for a long period of time.  
import RPi.GPIO as GPIO
import time
Green=18
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 

while 1:
    GPIO.output(Green, True)
    time.sleep(3)
    GPIO.output(Green, False)
    time.sleep(3)
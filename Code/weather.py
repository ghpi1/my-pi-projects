import pywapi
import string
import pprint
import time
import RPi.GPIO as GPIO
Green=18
Red=25
Blue=4
Button=24
Motion=17
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(Green, GPIO.OUT) 
GPIO.setup(Red,GPIO.OUT)
GPIO.setup(Blue,GPIO.OUT)
GPIO.setup(Button,GPIO.IN)
GPIO.setup(Motion,GPIO.IN)

weather = pywapi.get_weather_from_noaa('KLAX')
print "outside temp is", weather['temp_f']
if float(weather['temp_f']) < 32:
    print "cold"
    GPIO.output(Blue, 1)
    time.sleep(5)
    GPIO.output(Blue, 0)
  # Change the code so that instead of printing 'cold' turn the blue light turn on.
else:
   print 'hot'
   GPIO.output(Red, 1)
   time.sleep(5)
   GPIO.output(Red, 0)
   # Change the code so that instead of printing 'hot' turn the red light turn on. 